var intervalID = window.setInterval(updateScreen, 200);
var c = document.getElementById("console");

var txt = [
  "FORCE: XX0022. ENCYPTED://159.223.75.XXX",
  "TRYPASS: ********* AUTH CODE: ALPHA GAMMA: 1___ PRIORITY 1",
  "RETRY: REINDEER FLOTILLA",
  "Z:> /SCLL/UOC/GAME/NINJAS/ EXECUTE -PLAYERS 1 2 3 4",
  "================================================",
  "Priority 1 // local / scanning...",
  "scanning ports...",
  "BACKDOOR FOUND (159.223.75.000)",
  "BACKDOOR FOUND (159.223.75.001)",
  "BACKDOOR FOUND (159.223.75.002)",
  "...",
  "...",
  "CLL.EXE -r -z",
  "...locating vulnerabilities...",
  "...vulnerabilities found...",
  "MCP/> DEPLOY SCLL",
  "SCAN: __ 159.223.75.001",
  "SCAN: __ 159.223.75.050",
  "SCAN: __ 159.223.75.100",
  "SCAN: __ 159.223.75.110",
  "SCAN: __ 159.223.75.120",
  "SCAN: __ 159.223.75.130",
  "DECRYPTED://159.223.75.202",
];

var docfrag = document.createDocumentFragment();

function updateScreen() {
  //Shuffle the "txt" array
  txt.push(txt.shift());
  //Rebuild document fragment
  txt.forEach(function (e) {
    var p = document.createElement("p");
    p.textContent = e;
    docfrag.appendChild(p);
  });
  //Clear DOM body
  while (c.firstChild) {
    c.removeChild(c.firstChild);
  }
  c.appendChild(docfrag);
}
